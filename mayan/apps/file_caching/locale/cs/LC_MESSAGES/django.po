# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# trendspotter <jirka.p@volny.cz>, 2023
# Michal Švábík <snadno@lehce.cz>, 2023
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-16 23:30+0000\n"
"PO-Revision-Date: 2023-01-05 02:52+0000\n"
"Last-Translator: Michal Švábík <snadno@lehce.cz>, 2023\n"
"Language-Team: Czech (https://www.transifex.com/rosarior/teams/13584/cs/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: cs\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#: apps.py:26 events.py:6 permissions.py:6 queues.py:8 settings.py:11
msgid "File caching"
msgstr "Ukládání souborů do mezipaměti"

#: events.py:10
msgid "Cache created"
msgstr "Mezipaměť byla vytvořena"

#: events.py:13
msgid "Cache edited"
msgstr "Mezipaměť byla upravena"

#: events.py:16
msgid "Cache purged"
msgstr "Vyrovnávací paměť byla vymazána"

#: events.py:19
msgid "Cache partition purged"
msgstr "Oddíl mezipaměti vyčištěn"

#: links.py:26
msgid "File caches"
msgstr "Souborová mezipaměť"

#: links.py:31 links.py:37 links.py:41
msgid "Purge cache"
msgstr "Vyčistit mezipaměť"

#: model_mixins.py:45
msgid "Size at which the cache will start deleting old entries."
msgstr "Velikost, od které se v mezipaměti začnou mazat staré záznamy."

#: model_mixins.py:47 models.py:27
msgid "Maximum size"
msgstr "Maximální velikost"

#: model_mixins.py:54
msgid "Unknown"
msgstr "Neznámý"

#: model_mixins.py:64
msgid "Partition count"
msgstr ""

#: model_mixins.py:65
msgid "Total cached objects."
msgstr ""

#: model_mixins.py:70
msgid "Partition file count"
msgstr ""

#: model_mixins.py:71
msgid "Total cached files."
msgstr ""

#: model_mixins.py:88 model_mixins.py:274
msgid "Current size"
msgstr "Aktuální velikost"

#: model_mixins.py:89
msgid "Current size of the cache."
msgstr "Aktuální velikost mezipaměti."

#: model_mixins.py:276
msgid "Current size of the cache partition."
msgstr ""

#: models.py:21
msgid "Internal name of the defined storage for this cache."
msgstr "Interní název definovaného úložiště pro tuto mezipaměť."

#: models.py:22
msgid "Defined storage name"
msgstr "Definovaný název úložiště"

#: models.py:25
msgid "Maximum size of the cache in bytes."
msgstr "Maximální velikost mezipaměti v bajtech."

#: models.py:32 models.py:72
msgid "Cache"
msgstr "Mezipaměti"

#: models.py:33
msgid "Caches"
msgstr "Mezipaměti"

#: models.py:75
msgid "Name"
msgstr "název"

#: models.py:80 models.py:103
msgid "Cache partition"
msgstr "Oddíl mezipaměti"

#: models.py:81
msgid "Cache partitions"
msgstr "Oddíly mezipaměti"

#: models.py:106
msgid "Date time"
msgstr "Datum a čas"

#: models.py:109
msgid "Filename"
msgstr "Název souboru"

#: models.py:112
msgid "File size"
msgstr "Velikost souboru"

#: models.py:116
msgid "Times this cache partition file has been accessed."
msgstr ""

#: models.py:123
msgid "Cache partition file"
msgstr "Soubor mezipaměti"

#: models.py:124
msgid "Cache partition files"
msgstr "Soubory mezipaměti"

#: permissions.py:10
msgid "Purge an object cache"
msgstr "Vyčistit mezipaměť objektů"

#: permissions.py:14 queues.py:18
msgid "Purge a file cache"
msgstr "Vyčistit mezipaměť souborů"

#: permissions.py:17
msgid "View a file cache"
msgstr "Zobrazit mezipaměť souborů"

#: queues.py:13
msgid "Purge a file cache partition"
msgstr "Vyčistit oddíl mezipaměti souborů"

#: settings.py:17
msgid ""
"Number of times a cache will retry failed attempts to prune files to free up"
" space for new a file being requested, before giving up."
msgstr ""

#: settings.py:25
msgid ""
"Number of times a cache will attempt to prune files to free up space for new"
" a file being requested, before giving up."
msgstr ""
"Kolikrát se mezipaměť pokusí ořezat soubory, aby uvolnila místo pro nový "
"požadovaný soubor, než to vzdá."

#: views.py:50
#, python-format
msgid "Details of cache: %s"
msgstr ""

#: views.py:62
msgid "File caches list"
msgstr "Seznam souborů v mezipaměti"

#: views.py:83
#, python-format
msgid "Details of cache partition: %s"
msgstr ""

#: views.py:101
#, python-format
msgid "Purge cache partitions of \"%s\"?"
msgstr ""

#: views.py:118
msgid "Object cache partitions submitted for purging."
msgstr "Oddíly mezipaměti objektů odeslané k vyčištění."

#: views.py:127
#, python-format
msgid "%(count)d caches submitted for purging."
msgstr ""

#: views.py:128
#, python-format
msgid "%(count)d cache submitted for purging."
msgstr ""

#: views.py:136
msgid "Submit the selected cache for purging?"
msgid_plural "Submit the selected caches for purging?"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""
