# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Bedreddin Şahbaz, 2023
# serhatcan77 <serhat_can@yahoo.com>, 2023
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-16 23:30+0000\n"
"PO-Revision-Date: 2023-01-05 02:56+0000\n"
"Last-Translator: serhatcan77 <serhat_can@yahoo.com>, 2023\n"
"Language-Team: Turkish (Turkey) (https://www.transifex.com/rosarior/teams/13584/tr_TR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: tr_TR\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: apps.py:20 settings.py:11
msgid "Views"
msgstr "Görünümler"

#: forms.py:38
msgid "Search"
msgstr "Arama"

#: forms.py:54
msgid "Selection"
msgstr "Seçim"

#: forms.py:342
msgid "Label"
msgstr "Etiket"

#: forms.py:348
msgid "Relationship"
msgstr "İlişki"

#: generics.py:163
msgid ""
"Select entries to be removed. Hold Control to select multiple entries. Once "
"the selection is complete, click the button below or double click the list "
"to activate the action."
msgstr ""
"Kaldırılacakları seçin. Birden çok giriş seçmek için Ctrl tuşunu basılı "
"tutun. Seçim tamamlandığında, eylemi etkinleştirmek için aşağıdaki düğmeye "
"tıklayın veya listeye çift tıklayın."

#: generics.py:168
msgid ""
"Select entries to be added. Hold Control to select multiple entries. Once "
"the selection is complete, click the button below or double click the list "
"to activate the action."
msgstr ""
"Eklenecekleri seçin. Birden çok giriş seçmek için Ctrl tuşunu basılı tutun. "
"Seçim tamamlandığında, eylemi etkinleştirmek için aşağıdaki düğmeye tıklayın"
" veya listeye çift tıklayın."

#: generics.py:322
msgid "Add all"
msgstr "Hepsini ekle"

#: generics.py:329
msgid "Add"
msgstr "Ekle"

#: generics.py:340
msgid "Remove all"
msgstr "Hepsini kaldır"

#: generics.py:347
msgid "Remove"
msgstr "Kaldır"

#: generics.py:576
#, python-format
msgid "Error updating relationship; %s"
msgstr "İlişki güncellenirken hata oluştu; %s"

#: generics.py:583
msgid "Relationships updated successfully"
msgstr "İlişkiler başarıyla güncellendi"

#: generics.py:646
#, python-format
msgid "Duplicate data error: %(error)s"
msgstr "Yinelenen veri hatası: %(error)s"

#: generics.py:665
#, python-format
msgid "%(object)s not created, error: %(error)s"
msgstr "%(object)s oluşturulamadı, hata: %(error)s"

#: generics.py:676
#, python-format
msgid "%(object)s created successfully."
msgstr "%(object)s başarıyla oluşturuldu."

#: generics.py:721
#, python-format
msgid "%(object)s not deleted, error: %(error)s."
msgstr "%(object)s silinemedi, hata: %(error)s."

#: generics.py:731
#, python-format
msgid "%(object)s deleted successfully."
msgstr "%(object)s başarıyla silindi."

#: generics.py:794
#, python-format
msgid "Error retrieving %(object)s; %(error)s"
msgstr "%(object)s alınırken hata oluştu; %(error)s"

#: generics.py:906
#, python-format
msgid "%(object)s not updated, error: %(error)s."
msgstr "%(object)s güncellenemedi, hata: %(error)s."

#: generics.py:916
#, python-format
msgid "%(object)s updated successfully."
msgstr "%(object)s başarıyla güncellendi."

#: generics.py:983
#, python-format
msgid "%(count)d objects deleted successfully."
msgstr "%(count)d nesneleri başarıyla silindi."

#: generics.py:984
#, python-format
msgid "\"%(object)s\" deleted successfully."
msgstr "\"%(object)s\" başarıyla silindi."

#: generics.py:985
#, python-format
msgid "%(count)d object deleted successfully."
msgstr "%(count)d nesnesi başarıyla silindi."

#: generics.py:986
#, python-format
msgid "Delete %(count)d objects."
msgstr "%(count)d nesnelerini sil."

#: generics.py:987
#, python-format
msgid "Delete \"%(object)s\"."
msgstr "\"%(object)s\" sil."

#: generics.py:988
#, python-format
msgid "Delete %(count)d object."
msgstr "%(count)d nesnesini sil."

#: settings.py:17
msgid "The number objects that will be displayed per page."
msgstr "Sayfa başına görüntülenecek nesnelerin sayısı."

#: settings.py:23
msgid "A string specifying the name to use for the paging parameter."
msgstr "Sayfalama parametresi için kullanılacak adı belirten bir dize."

#: settings.py:29
msgid ""
"Display a submit button and wait for the submit button to be pressed before "
"processing up the upload."
msgstr ""

#: templates/views/app/foot.html:46
msgid "Cancel upload"
msgstr "Yüklemeyi iptal et"

#: templates/views/app/foot.html:47
msgid "Are you sure you want to cancel this upload?"
msgstr "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?"

#: templates/views/app/foot.html:48
msgid "Drop files or click here to upload files"
msgstr "Dosyaları sürükleyip bırakın veya yükleme yapmak için buraya tıklayın"

#: templates/views/app/foot.html:49
msgid "Your browser does not support drag and drop file uploads."
msgstr "Tarayıcınız sürükle ve bırak dosya yüklemelerini desteklemiyor."

#: templates/views/app/foot.html:50
msgid "Please use the fallback form below to upload your files."
msgstr "Dosyalarınızı yüklemek için lütfen aşağıdaki formu kullanın."

#: templates/views/app/foot.html:51
msgid "Clear"
msgstr "Temizle"

#: templates/views/app/foot.html:52
msgid "Server responded with {{statusCode}} code."
msgstr "Sunucu {{statusCode}} koduyla yanıt verdi."

#: templatetags/views_tags.py:30
msgid "Confirm delete"
msgstr "Silme işlemini onayla"

#: templatetags/views_tags.py:35
#, python-format
msgid "Edit %s"
msgstr "%s'ı düzenle"

#: templatetags/views_tags.py:38
msgid "Confirm"
msgstr "Onayla"

#: templatetags/views_tags.py:42
#, python-format
msgid "Details for: %s"
msgstr "Ayrıntılar: %s"

#: templatetags/views_tags.py:46
#, python-format
msgid "Edit: %s"
msgstr "Düzenle: %s"

#: templatetags/views_tags.py:50
msgid "Create"
msgstr "Oluştur"

#: view_mixins.py:413
#, python-format
msgid "No %(verbose_name)s found matching the query"
msgstr "Sorguyla eşleşen %(verbose_name)s bulunamadı"

#: view_mixins.py:439
#, python-format
msgid "Unable to perform operation on object %(instance)s; %(exception)s."
msgstr ""
"%(instance)s nesnesi üzerinde işlem gerçekleştirilemiyor; %(exception)s."

#: view_mixins.py:442
#, python-format
msgid "Operation performed on %(count)d objects."
msgstr "%(count)d nesneleri üzerinde gerçekleştirilen işlem."

#: view_mixins.py:443
#, python-format
msgid "Operation performed on %(object)s."
msgstr "%(object)s üzerinde gerçekleştirilen işlem."

#: view_mixins.py:444
#, python-format
msgid "Operation performed on %(count)d object."
msgstr "%(count)d nesnesi üzerinde gerçekleştirilen işlem."

#: view_mixins.py:445
#, python-format
msgid "Perform operation on %(count)d objects."
msgstr "%(count)d nesneleri üzerinde işlem gerçekleştirin."

#: view_mixins.py:446
#, python-format
msgid "Perform operation on %(object)s."
msgstr "%(object)s üzerinde işlemi gerçekleştirin."

#: view_mixins.py:447
#, python-format
msgid "Perform operation on %(count)d object."
msgstr "%(count)d nesnesi üzerinde işlem gerçekleştirin."

#: view_mixins.py:531
msgid "Object"
msgstr "Nesne"
